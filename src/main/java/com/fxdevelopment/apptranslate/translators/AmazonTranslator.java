package com.fxdevelopment.apptranslate.translators;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.services.translate.AmazonTranslate;
import com.amazonaws.services.translate.AmazonTranslateClient;
import com.amazonaws.services.translate.model.TranslateTextRequest;
import com.amazonaws.services.translate.model.TranslateTextResult;

import java.util.logging.Logger;

public class AmazonTranslator implements Translator {
    private static final Logger log = Logger.getLogger( "com.fxdevelopment.apptranslate" );

    private final String region;
    private final AWSCredentials awsCredentials;

    public AmazonTranslator( String region, AWSCredentials awsCredentials ) {
        this.region = region;
        this.awsCredentials = awsCredentials;
    }

    @Override
    public String translate( Language sourceLanguage, Language destinationLanguage, String phrase ) {
        AmazonTranslate translate = AmazonTranslateClient.builder()
                .withCredentials( new AWSStaticCredentialsProvider( awsCredentials ) )
                .withRegion( region )
                .build();

        if ( phrase.isBlank() ) {
            return phrase;
        }

        log.info( "Translating " + phrase + " from " + sourceLanguage.getLanguageCode() + " to " + destinationLanguage.getLanguageCode() );
        TranslateTextRequest request = new TranslateTextRequest()
                .withText( phrase )
                .withSourceLanguageCode( sourceLanguage.getLanguageCode() )
                .withTargetLanguageCode( destinationLanguage.getLanguageCode() );
        TranslateTextResult result = translate.translateText( request );
        String value = result.getTranslatedText();
        if ( phrase.contains( "%1" ) ) {
            value = value.replace( "% ", "%" );
        }
        return value;
    }
}
