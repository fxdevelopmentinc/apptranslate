package com.fxdevelopment.apptranslate.translators;

public interface Translator {
   String translate( Language sourceLanguage, Language destinationLanguage, String phrase );
}
