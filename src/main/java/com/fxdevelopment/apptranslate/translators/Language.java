package com.fxdevelopment.apptranslate.translators;

public enum Language {
   Arabic( "ar" ),
   ChineseSimplified( "zh" ),
   ChineseTraditional( "zh-TW" ),
   Czech( "cs" ),
   Dutch( "nl" ),
   English( "en" ),
   Finnish( "fi" ),
   French( "fr" ),
   German( "de" ),
   Hebrew( "he" ),
   Indonesian( "id" ),
   Italian( "it" ),
   Japanese( "ja" ),
   Korean( "ko" ),
   Polish( "pl" ),
   Portuguese( "pt" ),
   Russian( "ru" ),
   Spanish( "es" ),
   Swedish( "ev" ),
   Turkish( "tr" );


   private final String languageCode;

   Language( String languageCode ) {
      this.languageCode = languageCode;
   }

   public String getLanguageCode() {
      return languageCode;
   }

   public static Language parse( String value ) {
      try {
         return valueOf( value );
      } catch ( IllegalArgumentException iae ) {
         for ( Language language : Language.values() ) {
            if ( language.languageCode.equalsIgnoreCase( value ) ) {
               return language;
            }
         }
         throw iae;
      }
   }
}
