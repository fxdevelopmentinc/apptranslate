package com.fxdevelopment.apptranslate.formats;

public enum Format {
   PropertyFile,
   AppleStrings,
   AndroidStrings,
   ValueOnly,
}
