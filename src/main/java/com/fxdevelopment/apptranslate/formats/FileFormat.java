package com.fxdevelopment.apptranslate.formats;

import java.io.IOException;
import java.util.Map;

/**
 * Converts the file into a key-value pair.
 */
public interface FileFormat {
   Map<String, String> read() throws IOException;

   void write( Map<String, String> data ) throws IOException;
}
