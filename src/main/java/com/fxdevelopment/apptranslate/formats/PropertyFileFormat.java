package com.fxdevelopment.apptranslate.formats;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertyFileFormat implements FileFormat {
   private final String filename;

   public PropertyFileFormat( String filename ) {
      this.filename = filename;
   }

   @Override
   public Map<String, String> read() throws IOException {
      Properties properties = new Properties(  );
      try ( InputStream in = new FileInputStream( filename ) ) {
         properties.load( in );
      }

      Map<String, String> retval = new HashMap<>();
      for ( Object value : properties.keySet() ) {
         retval.put( value.toString(), properties.getProperty( value.toString() ) );
      }
      return retval;
   }

   @Override
   public void write( Map<String, String> data ) throws IOException {
      Properties properties = new Properties(  );
      for ( Map.Entry<String, String> entry : data.entrySet() ) {
         properties.setProperty( entry.getKey(), entry.getValue() );
      }

      try ( OutputStream out = new FileOutputStream( filename ) ) {
         properties.store( out, "" );
      }
   }
}
