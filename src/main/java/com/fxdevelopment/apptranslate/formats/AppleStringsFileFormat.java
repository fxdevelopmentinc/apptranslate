package com.fxdevelopment.apptranslate.formats;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class AppleStringsFileFormat implements FileFormat {
   private static final Logger log = Logger.getLogger( "com.fxdevelopment.apptranslate" );

   private final String filename;

   public AppleStringsFileFormat( String filename ) {
      this.filename = filename;
   }

   @Override
   public Map<String, String> read() throws IOException {
      Map<String, String> map = new HashMap<>();
      try ( BufferedReader bis = new BufferedReader( new InputStreamReader( new FileInputStream( filename ) ) ) ) {
         while ( true ) {
            String line = bis.readLine();
            if( line == null ) {
               break;
            }
            line = line.trim();
            if ( line.isBlank() ) {
               continue;
            }
            if ( line.startsWith( "/*" ) ) {
               continue;
            }
            line = line.replace( "\\r\\n", "\r\n" );
            String [] items = line.split( "\" = \"" );
            if ( items.length != 2 ) {
               log.info( "Could not properly split " + line );
               continue;
            }

            map.put( items[0].substring( 1 ), items[1].replaceAll( "\";$", "" ) );
         }
      }
      return map;
   }

   @Override
   public void write( Map<String, String> data ) throws IOException {
      try ( FileWriter fos = new FileWriter( filename ) ) {
         for ( Map.Entry<String, String> entry : data.entrySet() ) {
            fos.write( "\"" );
            fos.write( entry.getKey() );
            fos.write( "\" = \"" );
            fos.write( entry.getValue().replace( "\r", "\\r" ).replace( "\n", "\\n" ) );
            fos.write( "\";\n" );
         }
      }
   }
}
