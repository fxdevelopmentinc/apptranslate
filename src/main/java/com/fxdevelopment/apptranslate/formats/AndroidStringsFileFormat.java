package com.fxdevelopment.apptranslate.formats;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AndroidStringsFileFormat implements FileFormat {
   private final String filename;

   public AndroidStringsFileFormat( String filename ) {
      this.filename = filename;
   }

   @Override
   public Map<String, String> read() throws IOException {
      String data;
      try ( FileInputStream fis = new FileInputStream( filename ) ) {
         data = DataUtils.slurp( fis );
      }
      XmlMapper mapper = new XmlMapper();
      resources value = mapper.readValue( data, resources.class );

      Map<String, String> map = new HashMap<>();
      for ( string str : value.strings ) {
         if ( str.value == null ) {
            str.value = "";
         }
         map.put( str.name, str.value );
      }
      return map;
   }

   @Override
   public void write( Map<String, String> data ) throws IOException {
      ObjectMapper xmlMapper = new XmlMapper();
      resources items = new resources();
      items.strings = new ArrayList<>();
      for ( Map.Entry<String, String> entry : data.entrySet() ) {
         string str = new string();
         str.name = entry.getKey();
         str.value = entry.getValue();
         items.strings.add( str );
      }
      xmlMapper.writeValue( new File( filename ), items );
   }

   public static class string {
      @JacksonXmlProperty(isAttribute = true)
      String name;

      @JacksonXmlText
      private String value;
   }
   public static class resources {
      @JacksonXmlProperty(localName = "string")
      @JacksonXmlElementWrapper(useWrapping = false)
      List<string> strings;
   }
}
