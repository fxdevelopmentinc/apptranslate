package com.fxdevelopment.apptranslate.formats;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class DataUtils {
   public static String slurp( FileInputStream stream  ) throws IOException {
      StringBuilder data = new StringBuilder();
      byte [] read = new byte[ 4096 ];
      while ( true ) {
         int len = stream.read( read );
         if ( len == -1 ) {
            break;
         }
         data.append( new String( read, 0, len, StandardCharsets.UTF_8.name() ) );
      }
      return data.toString();
   }
}
