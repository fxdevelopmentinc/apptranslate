package com.fxdevelopment.apptranslate.formats;

public class FileFormatFactory {
   public static FileFormat create( Format format, String filename ) {
      switch ( format ) {
      case PropertyFile:
         return new PropertyFileFormat( filename );
      case AppleStrings:
         return new AppleStringsFileFormat( filename );
      case AndroidStrings:
         return new AndroidStringsFileFormat( filename );
      case ValueOnly:
         return new ValueOnlyFileFormat( filename );
      default:
         throw new UnsupportedOperationException( "Unknown format: " + format );
      }
   }
}
