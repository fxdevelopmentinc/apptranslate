package com.fxdevelopment.apptranslate.formats;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

/**
 * Useful for Spell Checking
 */
public class ValueOnlyFileFormat implements FileFormat {
   private final String filename;

   public ValueOnlyFileFormat( String filename ) {
      this.filename = filename;
   }

   @Override
   public Map<String, String> read() throws IOException {
      return Collections.emptyMap();
   }

   @Override
   public void write( Map<String, String> data ) throws IOException {
      try ( FileWriter fos = new FileWriter( filename ) ) {
         for ( Map.Entry<String, String> entry : data.entrySet() ) {
            fos.write( entry.getValue() );
         }
      }
   }
}
