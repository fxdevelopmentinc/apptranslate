package com.fxdevelopment.apptranslate;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.fxdevelopment.apptranslate.formats.FileFormat;
import com.fxdevelopment.apptranslate.formats.FileFormatFactory;
import com.fxdevelopment.apptranslate.formats.Format;
import com.fxdevelopment.apptranslate.formats.PropertyFileFormat;
import com.fxdevelopment.apptranslate.translators.AmazonTranslator;
import com.fxdevelopment.apptranslate.translators.Language;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * See README.md
 */
public class AppTranslate {
   public static void main( String[] args ) throws Exception {
      CommandLineParser parser = new DefaultParser();

      final Options options = new Options();
      Option intermediateIn = new Option( "a", "intermediateIn", true, "Intermediate file in the source language" );
      options.addOption( intermediateIn );

      Option intermediateOut = new Option( "b", "intermediateOut", true, "Intermediate file in the target language" );
      options.addOption( intermediateOut );

      Option inputFormat = new Option( "f", "format", true, "File Format (Defaults to Properties)" );
      options.addOption( inputFormat );

      Option input = new Option( "i", "input", true, "Input file name" );
      input.setRequired( true );
      options.addOption( input );

      Option output = new Option( "o", "output", true, "Output file name" );
      output.setRequired( true );
      options.addOption( output );

      Option sourceLanguage = new Option( "s", "source", true, "Source Language (Defaults to en)" );
      options.addOption( sourceLanguage );

      Option targetLanguage = new Option( "t", "target", true, "Target Language" );
      options.addOption( targetLanguage );

      Option region = new Option( "r", "region", true, "AWS Region" );
      region.setRequired( true );
      options.addOption( region );

      Option accessKey = new Option( "c", "accessKey", true, "AWS Access Key" );
      accessKey.setRequired( true );
      options.addOption( accessKey );

      Option secretKey = new Option( "d", "secretKey", true, "AWS Secret Key" );
      secretKey.setRequired( true );
      options.addOption( secretKey );

      AppTranslate translate;
      try {
         CommandLine commandLine = parser.parse( options, args );

         String intermediateInParam = null;
         if ( commandLine.hasOption( intermediateIn.getLongOpt() ) ) {
            intermediateInParam = commandLine.getOptionValue( intermediateIn.getLongOpt() );
         }
         String intermediateOutParam = null;
         if ( commandLine.hasOption( intermediateOut.getLongOpt() ) ) {
            intermediateOutParam = commandLine.getOptionValue( intermediateOut.getLongOpt() );
         }
         Format format = Format.PropertyFile;
         if ( commandLine.hasOption( inputFormat.getLongOpt() ) ) {
            format = Format.valueOf( commandLine.getOptionValue( inputFormat.getLongOpt() ) );
         }
         String source = "en";
         if ( commandLine.hasOption( sourceLanguage.getLongOpt() ) ) {
            source = commandLine.getOptionValue( sourceLanguage.getLongOpt() );
         }
         String target = null;
         if ( commandLine.hasOption( targetLanguage.getLongOpt() ) ) {
            target = commandLine.getOptionValue( targetLanguage.getLongOpt() );
         }
         String inputFile = null;
         if ( commandLine.hasOption( input.getLongOpt() ) ) {
            inputFile = commandLine.getOptionValue( input.getLongOpt() );
         }
         String outputFile = null;
         if ( commandLine.hasOption( output.getLongOpt() ) ) {
            outputFile = commandLine.getOptionValue( output.getLongOpt() );
         }
         String regionValue = null;
         if ( commandLine.hasOption( region.getLongOpt() ) ) {
            regionValue = commandLine.getOptionValue( region.getLongOpt() );
         }
         String access = null;
         if ( commandLine.hasOption( accessKey.getLongOpt() ) ) {
            access = commandLine.getOptionValue( accessKey.getLongOpt() );
         }
         String secret = null;
         if ( commandLine.hasOption( secretKey.getLongOpt() ) ) {
            secret = commandLine.getOptionValue( secretKey.getLongOpt() );
         }

         if ( target == null ) {
            throw new ParseException( targetLanguage.getLongOpt() + " is required" );
         }
         if ( inputFile == null ) {
            throw new ParseException( input.getLongOpt() + " is required" );
         }
         if ( outputFile == null ) {
            throw new ParseException( output.getLongOpt() + " is required" );
         }

         translate = new AppTranslate( intermediateInParam, intermediateOutParam, format, inputFile, outputFile, Language.parse( source ), Language.parse( target ), regionValue, new BasicAWSCredentials( access, secret ) );
      } catch ( IllegalArgumentException | ParseException e ) {
         System.out.println( e.getMessage() );
         HelpFormatter formatter = new HelpFormatter();
         formatter.printHelp( AppTranslate.class.getSimpleName(), options );

         System.exit( 1 );
         return;
      }

      translate.process();
   }



   private final String intermediateIn;
   private final String intermediateOut;

   private final Format format;
   private final String input;
   private final String output;

   private final Language source;
   private final Language target;

   private final String region;
   private final AWSCredentials awsCredentials;

   public AppTranslate( String intermediateIn, String intermediateOut, Format format, String input, String output, Language source, Language target, String region, AWSCredentials awsCredentials ) {
      this.intermediateIn = intermediateIn;
      this.intermediateOut = intermediateOut;
      this.format = format;
      this.input = input;
      this.output = output;
      this.source = source;
      this.target = target;
      this.region = region;
      this.awsCredentials = awsCredentials;
   }

   public void process() throws IOException  {
      Map<String, String> intermediateSource = new HashMap<>();
      if ( intermediateIn != null && new File( intermediateIn ).exists() ) {
         PropertyFileFormat format = new PropertyFileFormat( intermediateIn );
         intermediateSource = format.read();
         inverseMap( intermediateSource );
      }
      Map<String, String> intermediateTarget = new HashMap<>();
      if ( intermediateOut != null  && new File( intermediateOut ).exists() ) {
         PropertyFileFormat format = new PropertyFileFormat( intermediateOut );
         intermediateTarget = format.read();
      }

      FileFormat fileFormat = FileFormatFactory.create( format, input );
      Map<String, String> finishedValue = new HashMap<>();
      Map<String, String> input = fileFormat.read();
      Map<String, String> outputData = new HashMap<>();
      AmazonTranslator translate = new AmazonTranslator( region, awsCredentials );
      for ( Map.Entry<String, String> entry : input.entrySet() ) {
         // Look it up in the current file and if we can't find it, then translate.
         String translation = intermediateSource.get( entry.getValue() );
         if ( translation != null ) {
            translation = intermediateTarget.get( translation );
         }

         if ( translation == null ) {
            translation = translate.translate( source, target, entry.getValue() );
            intermediateSource.put( entry.getValue(), entry.getKey() );
            intermediateTarget.put( entry.getKey(), translation );
         }

         outputData.put( entry.getKey(), translation );
      }

      fileFormat = FileFormatFactory.create( format, output );
      fileFormat.write( outputData );

      if ( intermediateIn != null ) {
         inverseMap( intermediateSource );
         PropertyFileFormat format = new PropertyFileFormat( intermediateIn );
         format.write( intermediateSource );
      }
      if ( intermediateOut != null ) {
         PropertyFileFormat format = new PropertyFileFormat( intermediateOut );
         format.write( intermediateTarget );
      }
   }

   private void inverseMap( Map<String, String> map ) {
      Map<String, String> retval = new HashMap<>();
      for ( Map.Entry<String, String> entry : map.entrySet() ) {
         retval.put( entry.getValue(), entry.getKey() );
      }
      map.clear();
      map.putAll( retval );
   }
}
