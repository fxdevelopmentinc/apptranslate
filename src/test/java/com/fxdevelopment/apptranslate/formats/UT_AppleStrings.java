package com.fxdevelopment.apptranslate.formats;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UT_AppleStrings {
   @Test
   public void testStrings() throws IOException {
      String example = "\"PleaseWait\" = \"Loading...\\r\\nPlease wait...\";\n";

      File file = File.createTempFile( "AppleStringTest", "txt" );
      file.deleteOnExit();
      try ( FileOutputStream fos = new FileOutputStream( file ) ) {
         fos.write( example.getBytes( StandardCharsets.UTF_8 ) );
      }

      AppleStringsFileFormat entry = new AppleStringsFileFormat( file.getAbsolutePath() );
      Map<String, String> map = entry.read();
      assertEquals( 1, map.size() );
      assertTrue( map.containsKey( "PleaseWait" ) );
      assertEquals( "Loading...\r\nPlease wait...", map.get( "PleaseWait" ) );

      entry.write( map );

      String data;
      try ( FileInputStream fis = new FileInputStream( file.getAbsoluteFile() ) ) {
         data = DataUtils.slurp( fis );
      }
      assertEquals( data, example );
   }

   @Test
   public void testComments() throws IOException {
      String example = "\n" +
              "/* Class = \"UITabBarItem\"; title = \"HealthKit\"; ObjectID = \"33\"; */\n" +
              "\"33.title\" = \"HealthKit\";\n";

      File file = File.createTempFile( "AppleStringTest", "txt" );
      file.deleteOnExit();
      try ( FileOutputStream fos = new FileOutputStream( file ) ) {
         fos.write( example.getBytes( StandardCharsets.UTF_8 ) );
      }

      AppleStringsFileFormat entry = new AppleStringsFileFormat( file.getAbsolutePath() );
      Map<String, String> map = entry.read();
      assertEquals( 1, map.size() );
      assertTrue( map.containsKey( "33.title" ) );
      assertEquals( "HealthKit", map.get( "33.title" ) );

      entry.write( map );

      String data;
      try ( FileInputStream fis = new FileInputStream( file.getAbsoluteFile() ) ) {
         data = DataUtils.slurp( fis );
      }
      assertEquals( "\"33.title\" = \"HealthKit\";\n", data );
   }
}
