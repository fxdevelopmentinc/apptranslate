package com.fxdevelopment.apptranslate.formats;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UT_AndroidStrings {
   @Test
   public void testSimple() throws IOException {
      String example = "<resources><string name=\"resetPasswordHeader\">Forgot your password?</string><string name=\"signinIntro\">Get started by signing in!</string></resources>";

      File file = File.createTempFile( "AndroidStringTest", "txt" );
      file.deleteOnExit();
      try ( FileOutputStream fos = new FileOutputStream( file ) ) {
         fos.write( example.getBytes( StandardCharsets.UTF_8 ) );
      }

      AndroidStringsFileFormat entry = new AndroidStringsFileFormat( file.getAbsolutePath() );
      Map<String, String> map = entry.read();
      assertEquals( 2, map.size() );
      assertTrue( map.containsKey( "signinIntro" ) );
      assertEquals( "Get started by signing in!", map.get( "signinIntro" ) );
      assertTrue( map.containsKey( "resetPasswordHeader" ) );
      assertEquals( "Forgot your password?", map.get( "resetPasswordHeader" ) );

      entry.write( map );

      String data;
      try ( FileInputStream fis = new FileInputStream( file.getAbsoluteFile() ) ) {
         data = DataUtils.slurp( fis );
      }
      assertEquals( data, example );
   }
}
