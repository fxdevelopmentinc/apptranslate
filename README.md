# AppTranslate #

Using Amazon Translate, this will translate property and other files from one language to another language.  This uses an intermediate file in order to re-use strings from previous runs and other formats to control your costs.

The system is extensible to make it easy for you to add new formats or translators.

## Adding new File Formats ##

1. Create a new class which implements the com.fxdevelopment.apptranslate.formats.FileFormat interface
2. Add it to the com.fxdevelopment.apptranslate.formats.Format enumeration
3. Add support for it in com.fxdevelopment.apptranslate.formats.FileFormatFactory

## Adding new Translators ##

1. Create a new class which implements com.fxdevelopment.apptranslate.translators.Translator
2. Add support to instantiate it to com.fxdevelopment.apptranslate.AppTranslate

## Running the Application  ##

To run the application via Gradle

```
./gradlew :run --args="--help"
```

## Upgrades  ##

This uses Ben Manes version checker. You can check for updates via:

```./gradlew dependencyUpdates > /tmp/t ; grep ' ->' /tmp/t | sort | uniq```